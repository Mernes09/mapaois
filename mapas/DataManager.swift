//
//  DataManager.swift
//  mapas
//
//  Created by Bootcamp 8 on 2022-11-07.
//
import Alamofire
import Foundation
import GoogleMaps
protocol DataDelegado {
    func mostrarData(lista: [SensorResponse])
}
struct DataMaps{
    var delegate:DataDelegado?
    
    func verdatos (){
        let request = AF.request("https://rald-dev.greenbeep.com/api/v1/aqi")
            .validate()
            .responseDecodable(of: [SensorResponse].self) { (response) in
              guard let datoscorrectos = response.value else { return }
                delegate?.mostrarData(lista:datoscorrectos)
                
            }

    }
    
    
}
