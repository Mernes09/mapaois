//
//  ViewController.swift
//  mapas
//
//  Created by Bootcamp 8 on 2022-11-07.
//

import UIKit
import Alamofire
import GoogleMaps
class ViewController: UIViewController {
   // var mapasDa = DataManager()
    @IBOutlet weak var MapView: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        super.viewDidLoad()
                
          
                let camera = GMSCameraPosition.camera(withLatitude: -25.250, longitude: -57.536, zoom: 6.0)
                let mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
                self.view.addSubview(mapView)

                // Creates a marker in the center of the map.
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: -25.250, longitude: -57.536)
                marker.title = "Asuncion"
                marker.snippet = "Paraguay"
                marker.map = mapView
    }
    


}

 extension ViewController: DataDelegado{
    

    func mostrarData(lista:[SensorResponse]) {
        print(lista)
        DispatchQueue.main.async {
            let camera = GMSCameraPosition.camera(withLatitude: -25.2819, longitude: -57.635, zoom: 10.0)
            let mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
            self.view.addSubview(mapView)
            
            for i in 0..<lista.count{
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: lista[i].latitude ,longitude: lista[i].longitude)
                marker.map = mapView
                marker.title = lista[i].description
            }
        }
        //var datos = lista
        //print(datos)
    }
    

    //private func createMap(){
        //let camera = GMSCameraPosition.camera(withLatitude: -25.250, longitude: -57.536, zoom:zoomGet)
        //mapView = GMSMapView.map(withFrame:self.viewMap.frame, camera: camera)
       // mapView.isMyLocationEnabled=true
       // mapView.delegate=self
       // mapTheme()
      //  self.viewMap.addSubview(mapView)
 //   }
    //private func mapTheme(){
       // theme = UserDefaults.standard.string(forKey: "isDarkMode")
        //if(theme == nil){
          //  if self.traitCollection.userInterfaceStyle == .dark {
            //    do {
              //      if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                //        mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                  //  }
                  //}catch {
                    //NSLog("One or more of the map styles failed to load. \(error)")
                  //}
            //} else {
               // mapView.mapStyle = nil
          //  }
        //}else{
            //if(theme == "Dark"){
            //    do {
              //      if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                //      mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                   // }
                 // } catch {
                   // NSLog("One or more of the map styles failed to load. \(error)")
                  //}
            //}else{
              //  mapView.mapStyle = nil
          //  }
       // }
        
    //}
    
//}
 
